import { LightningElement } from 'lwc';
import IMAGE from "@salesforce/resourceUrl/graphic_home";

export default class GraphicHome extends LightningElement {
    homeImage = IMAGE;
}